package main

import (
  "os"
  "fmt"
  "net/http"
  "log"
)

func main() {
    port := os.Getenv("PORT")
    if port == "" {
        port = "8080"
    }

    fmt.Fprintf(os.Stdout, "Listening port:%s\n", port)
    hostname, _ := os.Hostname()
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	    fmt.Fprintf(os.Stdout, "Node: %s\n", hostname)
	    fmt.Fprintf(w, "Node: %s\n", hostname)
    })


    log.Fatal(http.ListenAndServe(":" + port, nil))
}

