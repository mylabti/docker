consul {
  address = "consul-server:8500"
  retry {
    enabled = true
    attempts = 12
    backoff = "250ms"
  }
}

template {
  source = "/consul-template-config/nginx.tpl"
  destination = "/etc/nginx/conf.d/default.conf"
  perms       = 0664
  command     = "service nginx reload"

}
