upstream backend {
{{ range service "http-ms" }}
  server {{ .Address }}:{{ .Port }};
{{ end }}
}

server {
   listen 80;

   location / {
      proxy_pass http://backend;
   }
}
